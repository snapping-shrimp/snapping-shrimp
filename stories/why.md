# Why

## TLDR;

- Open Source tools for remote work and remote collaboration.
  Curation / Creation of such tools.

## Longer version

Why did I create Snapping Shrimp group and why I plan to create many softwares
under this group? And what kind of softwares are these going to be?

I have been working in the software industry for about 3 years now. I used to
always go to office and work. Every day I commuted for at least 2 hrs. I used
to get pretty tired with the commute, especially the way back. And commuting was
not easy given the location I'm in which has lot of traffic, especially near
my workplace which is an IT hub and a lot of people leave work at around the
same time.

At around my second year at work I read the
[Remote: Office Not Required book](https://basecamp.com/books/remote). It was an
interesting book. I was only dreaming about remote work. I rarely worked from my
home though.

Fast forward to 2020, with COVID-19, everyone was forced to work from home if
work from home was possible. It's not a very ideal situation as everyone is just
getting forced to do it, as some may choose to go to office and may not like
remote work or may mix and match. There are many problems with remote work, if
not done well especially. And there are also some downsides to remote work. A
lot has been said about remote work. Personally, what I have seen is - the rise
of lot of tools to help with remote work and tools to help with the functioning
of teams and offices given the COVID-19 situation. I mean, a LOT of tools and a
lot of features. These tools were probably very normal stuff before. Now it's
like the base of many companies which have opted work from home for most of
their employees.

Personally, I have been using Zoom and Microsoft Teams tool in the recent times.
These are all closed source tools, and mostly paid tools. There are also some
more closed source tools but free ones. I have seen few Open Source
alternatives to help with some of the features, like Screen Sharing, Audio call,
Video call, Instant Messaging. I haven't checked out much about remote mouse
control.

I also noticed that tools like VS Code Live Share are closed source. But there
are very few Open Source extensions and tools available for VS Code and other
editors. I used [Atom Teletype](https://teletype.atom.io/) alone a bit and
liked it very much! :)

My ambitious aim is to create an Open Source tool like VS Code Live Share,
starting with the basic features and maybe slowly expand to more text editors
and IDEs and then to complete desktop like Zoom, [Tuple](https://tuple.app/)
and other similar tools. Most of my focus will be on collaboration - a field
where a lot of research has already been done and there are probably many tools
too - I have to do my research, but my feeling is that there is a lot of
research and software available for Audio Call, Video Call, basic Screen
Sharing, Instant Messaging. And this includes Open Source tools. But for other
features like Collaborative editing - I have not noticed any Open Source tools
but more of closed source tools like Google's set of Apps, Mural and tons of
others. Maybe I need to do my research well. 🤷

I have many ideas. I'll try my best to not reinvent the wheel and always go with
existing Open Source tools. I'll probably be doing lego work then, instead of
writing everything from scratch.

I do know this to start with - there are not many extensions within text
editors and IDEs that have features like VS Code Live Share. Even if there are
a few, they are not Open Source tools.

My aim is to also learn something from these projects about networking and
collaboration over the Internet.

If I do end up trying to create a Zoom alternative, I hope that I create a tool
that does not eat up the computer's resources - not sure if that's possible - I
hope it is. Currently, when I use Zoom / Microsoft Teams app and do screen
sharing, my Mac becomes too slow. It's really astonishing and annoying too! I
hope that the tools I create are not annoying the user - I'll strive for it.

Snapping Shrimp will be the brand name under which I'll be creating tools for
remote work and remote collaboration. I also plan to attract many contributors
for the Open Source tool to cocreate it! :)
