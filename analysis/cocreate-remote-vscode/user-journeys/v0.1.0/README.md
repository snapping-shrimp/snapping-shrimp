# User Journey

Since the collaboration is between two users. Some parts of the journey are
the same for both users

User 1

- User opens VS Code
- User opens an existing file in a tab
- User clicks on `Extensions` section
- User searches for remote pairing tool, maybe explicitly the `Cocreate Remote`
  tool
- User clicks and chooses the `Cocreate Remote` extension
- User checks out the extension details with text and images (static and GIFs)
- User installs the extension and enables it for all workspaces
- User opens up `Command Pallete` using keyboard shortcut or through menu
  options
- User chooses the option `Cocreate Remote: Share and Cocreate`
- There's a message saying that the user is not signed in and that they need
  to sign in first
- User is asked to open a link for sign in
  - On opening the link, it says "Sign In with GitHub"
  - User signs in with their GitHub account
- In VS Code, the user is asked to paste a token in an input box that is
  open on the top middle, the area where `Command Pallete` usually appears.
- User is shown a message that they are sharing their file and that they can
  cocreate with their friends by sharing a link
- User shares the link to their friend

User 2 (User 1's friend)

- User opens VS Code
- User clicks on `Extensions` section
- User searches for remote pairing tool, maybe explicitly the `Cocreate Remote`
  tool
- User clicks and chooses the `Cocreate Remote` extension
- User checks out the extension details with text and images (static and GIFs)
- User installs the extension and enables it for all workspaces
- User opens up `Command Pallete` using keyboard shortcut or through menu
  options
- User chooses the option `Cocreate Remote: Join my friend`
- There's a message saying that the user is not signed in and that they need
  to sign in first
- User is asked to open a link for sign in
  - On opening the link, it says "Sign In with GitHub"
  - User signs in with their GitHub account
- In VS Code, the user is asked to paste a token in an input box that is
  open on the top middle, the area where `Command Pallete` usually appears.
- Now the User is shown an input asking to paste a link that they got from their
  friend
- User pastes the link they got from their friend
- User sees the file that their friend has opened
- User is able to see the contents of the file, but not able to edit it.
- User is able to keep their cursor on the file
- User is able to see their friend's cursor on the file
- User is able to interact with the file, like - use language features to see
  the definition of a symbol

User 1

- User sees that their friend has joined
- User sees two cursors on the file, where one cursor is automatically moving
  and understands that their friend is controlling that cursor
- User has a cursor for themselves that they use to type into the file

---

Value provided to the user?

- The remote user is able to see the file of their friend and able to see in
  real time what they are doing. How is this better than Google Docs? In VS
  Code, the remote user will also be able to try out the editor specific
  features, like language features. It's more suited for coding, unlike Google
  Docs. And this is also better than screen sharing tools as they are heavy, to
  just show a single file, but yes, this is only read only access as of now.

---

TODO:
The above user journey does not go too much into the detail of how the sign in
would look like and how some messages are conveyed to the user in detail, like
how exactly it will look like in the VS Code UI. So that's something to improve
on. There's also no mention of which platform the user is on - which Operating
System. If we are keeping it cross platform like how VS Code is, then any code
that we write for CoCreate Remote should not be OS specific and should instead
be OS agnostic.

Ideas:

- For Sign In, we can support GitHub Sign In to start with.

Questions:

- Can User 2 see User 1's selection? Selection of a word or a group of words.

Ideas for the next version User Journey:

- Given the user is able to see a single file now, we can either move on to
  multiple files - with or without the concept of workspaces, or move on to
  showing the terminal of the remote user! :) All of them still being only read
  access! It's almost as good as simple screen sharing without remote write
  access, but it's probably fast enough and better as it's built inside the
  editor and native. Unlike Zoom and other similar screen sharing tools.
