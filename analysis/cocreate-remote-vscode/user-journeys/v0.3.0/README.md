# User Journey [WIP]

Since the collaboration is between two users. Some parts of the journey are
the same for both users

User 1

- User opens VS Code
- User opens an existing file in a tab
- User clicks on `Extensions` section
- User searches for remote pairing tool, maybe explicitly the `Cocreate Remote`
  tool
- User clicks and chooses the `Cocreate Remote` extension
- User checks out the extension details with text and images (static and GIFs)
- User installs the extension and enables it for all workspaces
- User opens up `Command Pallete` using keyboard shortcut or through menu
  options
- User chooses the option `Cocreate Remote: Share and Cocreate`
- There's a message saying that the user is not signed in and that they need
  to sign in first
- User is asked to open a link for sign in
  - On opening the link, it says "Sign In with GitHub"
  - User signs in with their GitHub account
- In VS Code, the user is asked to paste a token in an input box that is
  open on the top middle, the area where `Command Pallete` usually appears.
- User is shown a message that they are sharing their file and that they can
  cocreate with their friends by sharing a link
- User shares the link to their friend

User 2 (User 1's friend)

- User opens VS Code
- User clicks on `Extensions` section
- User searches for remote pairing tool, maybe explicitly the `Cocreate Remote`
  tool
- User clicks and chooses the `Cocreate Remote` extension
- User checks out the extension details with text and images (static and GIFs)
- User installs the extension and enables it for all workspaces
- User opens up `Command Pallete` using keyboard shortcut or through menu
  options
- User chooses the option `Cocreate Remote: Join my friend`
- There's a message saying that the user is not signed in and that they need
  to sign in first
- User is asked to open a link for sign in
  - On opening the link, it says "Sign In with GitHub"
  - User signs in with their GitHub account
- In VS Code, the user is asked to paste a token in an input box that is
  open on the top middle, the area where `Command Pallete` usually appears.
- Now the User is shown an input asking to paste a link that they got from their
  friend
- User pastes the link they got from their friend
- User sees the file that their friend has opened
- User is able to type in the file using a cursor assigned to them

User 1

- User sees that their friend has joined
- User sees two cursors on the file, where one cursor is automatically moving
  and is also typing things and understands that their friend is controlling the
  cursor
- User has a cursor for themselves that they use to type into the file
