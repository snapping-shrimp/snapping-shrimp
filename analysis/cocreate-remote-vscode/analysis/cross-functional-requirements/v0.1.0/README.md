# Cross Functional Requirements

For v0.1.0 , the priority is to have the app up and running, not thinking much
about the Cross Functional Requirements. But if possible, consider the following

Check https://github.com/karuppiah7890/opensource/blob/master/ideas/remote-pairing-tool/cross-functional-requirements.md for basic details on cross
functional requirements with respect to remote pairing tools

- Cross Platform
  Users can use Linux, MacOs and Windows. These are Operating Systems that
  VS Code supports by default. And architecture? We will support 64 bit
  architecture by default.

  Note: VS Code supports a lot of architectures. Check more here
  https://code.visualstudio.com/#alt-downloads
