# Guest

Guest usually in the software system world means, a user from outside and not
exactly part of the system or organization. It also refers to some sort of role,
where the user has to sign in and they get identified and their access is of
that of a guest.

Another concept of guest is - no sign in. So, the user does not even have an
identity in the system.

Sometimes users may not want to sign in, due to reasons like hassle in sign in
through GitHub or any platform that we are supporting, or any other reason. So,
the user sharing their work can also say that "Hey, my friend wants to join as
guest", so yeah this could be a feature too. But the dangerous part is, the
user sharing will not know if the other user is really their friend as they are
not signed in and have been given access directly. It's not exactly secure, but
it's up to the users using the extension!
