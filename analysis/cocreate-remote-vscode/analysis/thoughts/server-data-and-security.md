# Server Data and Security

Having very less data in the server would be an ideal situation. As it will mean
less assets to protect.

Also, do we need email IDs? It's a burden to have email IDs in database. Then
if it's hacked, it's a problem. Threat Modelling needs to be done!! :) Start
with no email ID for now! :) Is that possible? Like, no permissions from GitHub
needed. What if we change our minds tomorrow? They can sign-in again. Simple. :)

Code data going through servers can also be a problem, in case we choose such
an implementation.
