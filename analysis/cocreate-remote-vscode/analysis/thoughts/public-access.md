# Public Access

This is regarding public access to the extension and any service we build and
provide.

We cannot allow lot of public access at the same time. For example, if we use
free Heroku instance, only some amount of DB quota will be there. I can't spend
money for public instances. So yeah. I need configuration or code in the
extension and server side, to have a limit of users or records or something
like that. Whatever is hit first, records or users, we stop then. Have to check
how Heroku and others have quota. Usually MB and stuff or records or both. I
think MB is the main thing, the disk size. Records is just an estimate based on
that disk size I think. So yeah, we need to have limits for users logged in and
using the service and also for time - or else people can hog the service.
Automatic kick out or logout after a few minutes. Probably a warning a few
minutes before kickout will be nice. This setting has to be at server level and
extension level too maybe? Idk

This is purely based on the fact that, we ATLEAST need servers to authenticate
and discover people. Gotta see if there are other free services that can help
with that. Even then, there are usually limits

This special configuration can be enabled only in our public servers. If users
host their own server, they can switch off all the limiting features.
