# Configurability

## Private or Self hosted instance

The user should be able to install our extension from the public marketplace and
connect to their private or self hosted server instance for using the extension.

## Switching between server instances

This would mean that a user can store the data about multiple server instances
and switch between them. What would be the use case for this? Org work vs
public work like open source?

If this feature is not present, people would just need to note down the
configuration somewhere and keep changing it's value in the extension if
extension can only store data about one server. This is assuming the fact that
only server URL will be a configuration. It's possible that we bring in more
configurations for different instances - relating to the server, or even to the
extension. Not sure if we connect both server and extension as one - will it
be a good thing or not.

### Context Switching

This is one of the terms used for such concepts. Context can define a server
instance configuration, or a combination of server instance configuration and
client configuration.

For example, a user could have

- Work context - work server, specific theme for extension and some other
  properties
- Public Open Source context - public server, specific theme for the extension
  and some other properties

Why change themes? Something as basic as knowing which server and context you
are using would help through the theme. Or the context name can be clearly
shown to the user. And for every session, the user can be shown what context
they are using when sharing and also when user connects to their computer or
versa.

## Default configurations

Every configuration that we define - we can have sensible defaults so that the
user can use the extension with ease. And it will also be easy for them to
understand and change the configuration later if needed. This is basic
etiquette
