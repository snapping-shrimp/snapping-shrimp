# Invite

Send invite to friend with just GitHub ID or any uniquely identifying thing,
like email. No need to open another chat application to share the link to join
:) This might be a tough feature. Especially if the remote user has logged in
into multiple VS Code instances. Only one can accept I guess. For others, idk,
maybe make it invalid? Or let them in too. As long as they are logged in and
identified themselves with their credentials. How to differentiate multiple
computers then? As they will have same user ID. Check about this! :) For now we
have only two users however. We will keep this as a hard restriction for now.
We can think about scaling to more users slowly and steadily.
