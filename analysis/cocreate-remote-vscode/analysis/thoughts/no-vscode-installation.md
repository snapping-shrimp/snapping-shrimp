# No VS Code Installation

This is particular case where we provide the user with a feature that they don't
even have to install VS Code - mainly the user joining a remote user, but it can
also hold good for a user sharing their work too.

How? We will provide all the features online, through the web browser.

This can also be seen as - Online version of Cocreate Remote. Where one way is
through VS Code! :) But how?

Hosted instance of VS Code in a server. I have seen Microsoft do it. So, people
don't even have to install VS Code in their machines then.

Idk how it will work, if VS Code web app will be standalone and fully client 
side static app with almost no help from server or if it will use some server 
resources for anything.

If it's delegating any work to server, then that's something to notice.

With respect to Cocreate Remote, we might use some technologies that are OS 
dependent, for example, store any secrets in the key ring. But browser 
standalone app won't have that. I think the extension will be running on 
client? Or server? Idk. I guess server? So yeah, that's something to note. If 
it's able to run on client side, then extension has to store password 
differently by first detecting it's a different environment.

We might also use WebRTC. WebRTC is actually a browser based technology. But 
when used in Electron app, in VS Code, we gotta see what we use - we might be 
using the node runtime WebRTC. So, depending on the environment, again we have 
to see what to do.

This is an interesting use case. But something that requires a lot of thought. 
Also, we still need to understand the hosted VS Code instance and also, we 
haven't taken the above decisions to build the app, so we don't know how things 
will turn out.

sAnother thing to note is, we will also try to store session data somewhere.
