# Permission to join session

No matter the user journey, it's inevitable that at some point, one user is
going to join another user and collaborate and work.

Now, before joining, the user sharing the work could actually be asked if they
want to let someone join them. This could be a good practice as - this way, the
user is alerted about someone joining them, and they are NOT joined until the
user says so. This seems like a good control that the user might need. This
could be a default, and could be configured with a recommendation to may be
keep the default for satefy reasons. For any one joining the session, only if
user allows, they can join and then access the data, till then, they can't do
anything. This is kind of like the Zoom Waiting Room feature where Admin admits
the people present in the waiting room :)

So, the user journey might have something like - show notification that friend
is trying to join and allow them only if the user says so.

If it seems like a hassle to do it again and again and again, then we can have
another feature called Trusted users.

Trusted users - for whom we don't have to allow again. Some place to manage
already trusted users would be good too, in case we want to untrust them.
This is helpful when the same set of people work together a lot and no one has
trust issues and no one is concerned about any other security attacks. I mention
security attack because - trusted users means - we have a set of user IDs of
some sort. If a hacker is able to masquerade as that user for some reason or is
able to hack that person's account, then with the trusted user concept, they get
allowed into the session even if the user sharing the session was not expecting
them.

A good thing would be to show notifications to the sharing user every time a
user joins a session :) At least this way, there won't be any surprises.
