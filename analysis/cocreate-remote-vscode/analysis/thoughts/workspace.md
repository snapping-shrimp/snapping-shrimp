# Workspace

If VS Code is already open in the user's computer

New window (new workspace) vs same window (existing workspace) when opening
friend's link for remote pairing

Same window (same workspace) is very straightforward when the user is sharing
only one file. How do we show it in the file explorer though? Check how VS Code
shows files from unrelated directory in a different workspace

The concern is, we are mixing up existing workspace along with remote pairing
workspace. The user may or may not like this. For example, when doing projects
level search, it will search everywhere. Actually, in this case, if it's only
one file, for simplicity, the user can search that file itself with file level
search. Also, even with one file, how do we tell the user that - that one file
in the workspace is a remote pairing file??

Thinking about the future, clearly when sharing whole workspaces, the user
would not want to mix things up. Again, with project level search, it will
search two workspaces and that's mixing up things. Also, we need to again
differentiate between local workspace vs remote shared workspace and show them
to the user in the file browser/explorer on thr left.

I think separate window and hence separate workspace is better, if the user has
already opened up an instance of VS Code.

What if the already existing one VS Code is an empty workspace? Maybe we can
just reuse it? But if there are many instances, not sure if we can detect which
one is empty workspace if the user's link input is obtained on a workspace
that's not empty.

Straight forward thing is - always open new workspace :) Try in first version.
Or we can do this later. But this looks like the future! Seeing the pros and
cons

Something to think about when doing development! :)
