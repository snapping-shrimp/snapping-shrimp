# Cost

Cost solely depends on our architecture, which is not yet solutionised or
decided.

Some things to consider -

- Server - RAM, CPU
- Database - RAM, CPU, Disk Size
- Number of users
- Length of a pairing session

If you think about it, when very few people are using the service and at
only small lengths, we would expect the cost to be low, compared to more
people or longer lengths. It would be great if cost does NOT depend on the
length of the session too.

These are some things to consider while architecting the solution! :) If we
think that the usage of server will be less - an architecture that is both
serverless friendly and server based architecture friendly can help - so that it
can be used in both cases. This is mostly because I have heard that serverless
can get costly if used almost like a normal server architecture - in such cases
long running and always running servers can be preferred. Again, something to
note when architecting solutions!
