# Data [WIP]

> Note: This is all still a speculation as we have not finalized anything or
> written any code to concretely say what we will store

## Where to Store?

Idea is to avoid storing data in the server as much as possible. This will be
important when it comes to data laws and also for security reasons, it's better
this way! :)

All or most session data is stored in the client machines. Only session
metadata is present in the server to help with discovery of friends and
connecting with them.

## How to store?

What about storage part? For now no need for CRDT as it's going to be
read-only only for the first version. So, yay!! :)

But for later, we will need some sort of CRDT for collaborating on text
documents - like code!

## What data do we have?

What does a session mean? Here I refer to session as a period time where two
or more users are collaborating and working

What does session data mean? This might be something like - the discussions that
the users have on different lines of code, chat messages, or any data that is
specific to a session where many features are available through VS Code or
through the extension and might be stored in case it's important for the
session

What does session metadata mean? This might be something like - the IDs of the
users who are collaborating on a particular session, the session ID itself and
similar data.

## Preventing data loss for the user

What happens when one of the two people lose Internet connection in between?
Can we resume session once Internet is back? Consider this case for both the
users - one who has started the sharing and the one who joined the session
through link or invite. What happens when both lose Internet connection? One
way to be able to resume when Internet is back is to ensure both have the
session data in their local machines and once they connect back, they share
this data and then move on. This is based on Conflict Free Replicated Data
Type. The one main thing to ensure is, the server has the session metadata, and
the users also have the session metadata. Only then we can easily connect back
and resume, considering the data stored in the local is stored with the session
metadata as key, for example session ID. Or, we could just always store a max
of only one session and reuse that. Both has pros and cons. Session ID helps
when we have multiple VS Code instances running and each having a session
running in it and when data is stored in the file system and not in-memory. A
good question to ask is, what if one of the user's system crashes? Or they
close VS Code by mistake. If they join again using link, they might get a new
session ID right? Since it's kind of like a new session. That's something to
note. I guess it's okay if they get new session ID, or maybe they reuse the
existing session since the other user still has VS Code up and running and
doesn't matter if there was an Internet glitch. But if we both close VS Code in
some way, for example crash etc, it's hard to ensure 0 data loss. We can try
out best to retrieve last stored data and show the user id they want to use
that and continue the session. What is this data? For example, if the two users
were discussing something about the code with discussion per line, then that's
something to keep. If we have chat feature, we should keep that data till user
explicitly closes session I think. Since it's all present in client local
machine, I guess it's okay. The only advantage of having data in server is the
usual thing - if both clients crash, and machine is corrupted, the data is
gone. If for some similar reason, client's data is lost, server can provide
backup or be the source of truth. But still, this might be confidential data.
So yeah. Maybe we should just let the user decide? By default, store as little
data as possible in server. :P :) It's better for public usage! :) Data will
not even pass through the server ! :)
