# Access to Code

Analysis on an alternative way of remote pairing. Both the users have the 
codebase in their local. Benefits and pit falls of remote pairing in such a 
situation by having the benefit of using the local codebase directly instead of 
accessing another person's remote codebase. Especially for large codebases 
(with lot of git history), this might have lot of benefits. Pit falls - one 
thing is, it's better if both of them are on the same git commit sha when 
starting off. Also, when making changes, will it be done to both local and 
remote user repo? What if they disconnect for some time? Hmm. How to sync? And 
isn't this duplication? Lot of possible issues there. Or, maybe one can be 
considered as the source of truth and all changes can go into that user's repo, 
the other user will get ONLY the changes and the whole repo will be a mix of 
local repo + changes. Changes will be separately kept and not done for this 
user. But the pitfall due to that is, in case source of truth is gone, this 
person should still be able to do a commit, as they also have access to the 
code now. Is that possible? Hmm.

Now we are also talking about git level stuff. Version control stuff. Ideally, 
just getting the codebase, the latest version is fast, when other person does 
not have codebase. Hmm. But getting git history? They can get that too from 
main user :) can remote user move to a different branch or git checkout in VS 
code? Hmm, that's like any other action, which is done remotely. So, I think 
it's possible. Language features too are similar or are local features based on 
local extensions? Hmm
