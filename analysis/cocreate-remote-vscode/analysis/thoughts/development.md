# Development

Read other extension code, CI CD, publishing, packaging, testing!! Check what
are the best practices :)

## Writing Code

Aim should NOT be to write code and maintain it. Aim should be to help people
to pair and that too, as soon as possible and also securely. So, ideally, it's
best to write as little code as possible, mainly for the core product alone,
and try to delegate to other Open Source libraries, tools and softwares as much
as possible

Aim is to keep everything or at least most things Open Source.

## Coupling

Everything loosely coupled through interfaces. Mock out stuff. Keep the
interfaces simple and good. Try to not have breaking changes. Understand good
design. Try to ensure things can evolve.

## Paradigm

Functional programming vs object oriented programming. Check Niel Ford talk
maybe? Understand what it means to build in a functional manner and what kind
of problems it can solve and make things easy.

## Input and breaking changes

Passing input to functions. Extra input means breaking change in types world.
Hmm. It could be optional inputs. Or, a nice single object with multiple fields
as inputs ;)

## Using External libraries

Wrap up usage of libraries with custom modules / components which are like an interface for our core code. This interface only gets data that the app gives as input and the wrapper can take care of other stuff. This is also useful when changing libraries for the same feature, without having any changes in the interface or the core code, including breaking changes. Do this at least for the main source code. For test code, this is going to be hard, as we will be using testing framework(s) and frameworks cannot be wrapped easily and will also compromise the features provided by the framework. So, for test code and framework, we don't need to build any wrapper around it.

Wrapper based on API or the feature the API provides, vs wrapper based on my need for the application and it's input and necessary output. Hmm.

Internal wrapper not for external people as this can easily keep changing. Hmm.

Gotta see how to ensure it's not exposed to external but exposed to internal need. Or just tell people it's not for public use. Simple. :) As it's just a wrapper. Hmm

Wrapper is only for what features or stuff we need currently. Not for future or for wrapping all APIs and stuff. Just one thing? Just one wrapper. And so on.
