# Read Write Access

Write access - can create directories and files? And write to them too?

Read only access - can see a non interactive view of the file? Or interactive
but non editable view? So they can keep cursor, go to definition, peek
references etc, but when it comes to editing, it gives error when tried to save
or even edit. It doesn't edit basically. At least the source doesn't get
changed. Since this is all live, edit should not even be possible, and hence
even save after edit, as edit itself not possible. It should show error and say
that user has read only access. Maybe they can request the remote user for
write access.

Once write access is given, can we revert back to read access on the fly? :D
how to control things at that time? Hmm. Immediately stop other user from
editing or saving file! As soon as possible atleast

More fine grained control? Write and read access for different things. Write to
file. Create file or directory. Write access to terminal. Hmm.

Can't Role based access control here, so meh. Explicit manual access for now.
Rbac is a thing for later and still a user config I guess.
