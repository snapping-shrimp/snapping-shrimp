# Sign In

Do we really need Sign In with GitHub?

What does it help in?

When users invite other users and interact with each other, it's better if the
system knows who each user is exactly - that is, their identity. One of the ways
to get identity is to ask the user to login. A pretty usual way. And to avoid
user creating a new account with new credentials, we can piggy back on existing
platforms like GitHub, Google among others.

Will we need a server for this? Looks like that.
