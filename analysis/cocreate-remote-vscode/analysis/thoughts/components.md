# Components

Roughly I see the following problems to be tackled and there are one or more
components needed for each of these problems

View - User Interface, User Experience

Networking - How networking happens between the two or more users?

Data store - How and where is data stored?

Resources - How much CPU and RAM is utilized by the extension / software we use?

Speed - How is the speed of the extension? This relates a bit to Resources.
Ideally the extension should work fast even with less resources. Working fast
with more and more resources is just usual now a days and doesn't make so much
sense.

Security - How secure is the whole collaboration session?

Collaboration - How is the collaboration aspect of the extension? It's the key
thing of the extension.

Seamless - Is the whole experience seamless or does it have any hassles?
