# Need for Server?

Do we need a Server?

In any case, a server with DB, with GitHub IDs and maybe email IDs, of logged in
people, will be needed. It will be like a bootstrap server, for people to
discover and connect to each other. The server can maintain some metadata about
the pairing session, for example, some unique code for the session, which will
be part of the unique link that users can share to share their session.
And that's it maybe, no other data in server.

But if we choose an implementation for our extension where every communication
between two users has to go through our servers, then we might have a problem.
This also means that we will need more servers in such cases.

Is our code data going to go through servers? Hmm. Not sure. What is the ideal
situation? Whatever is the fastest and most secure way. If that's through
server, gotta do it. But server will become a bottle neck too then. Hmm. Yes.
We have to have lot of instances then, yeah.
