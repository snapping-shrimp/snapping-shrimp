# Multiple Identities. Multiple GitHub accounts or other accounts

If we allow multiple contexts. Could we also allow multiple GitHub sign in? And switch between different accounts with ease? Hmm. This might be useful if some people want to simply use work account for some reason, in some cases and personal account in other cases. Maybe possible that the backend server is a private service and allows only email IDs with particular domain name, or only some GitHub IDs, say, belonging to an organization, etc or part of a list it has etc.

In such cases, when people want to switch to a different backend service with ease, they might want to use personal account. Hmm.

I should try to make sure that everything is pretty flexible and not too tight. Hmm. And also try to ensure backward compatibility. Lol. Or else problematic when doing upgrades. Hmm. Think about how to support just one account for now, but maks sure it's flexible for multiple accounts too!! :)
