# Packaging

Check how other extensions do it

## Bundling

Parcel? Webpack? Others? Choose one.

Minification. Bundling. Transpilation from ts to js with tsc.

## Ignore unnecessary files

When you bundle the extension and package it, how will you know it does NOT 🚫
contain tests? How to verify?

VS code ignore file / config, similar to gitignore

How to check package contents? To make sure the content in it is minimal and
appropriate?

We can actually find out the files that will be packaged and published by using
this command -

```bash
$ vsce ls
```

An example is below

```bash
$ vsce ls
Executing prepublish script 'npm run vscode:prepublish'...

> cocreate-remote@0.0.1 vscode:prepublish /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode
> npm run compile


> cocreate-remote@0.0.1 compile /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode
> tsc -p ./

CHANGELOG.md
docs/acceptance-criteria.md
docs/pre-release-handbook.md
docs/testing/manual-testing.md
out/extension.js
package.json
README.md
```

## Security

Due to security, VS Code marketplace has some restrictions

Something to check on:

- Image Link - HTTPS only
- Images

Reference: https://code.visualstudio.com/api/working-with-extensions/publishing-extension#publishing-extensions

## Marketplace integration

This is about how the extension shows up in the marketplace in the web and
inside VS Code :)

https://code.visualstudio.com/api/working-with-extensions/publishing-extension#marketplace-integration

https://code.visualstudio.com/api/references/extension-manifest#marketplace-presentation-tips

This has to be done as part of coding, as a finishing touch for packaging
